﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform _target;
    public GameObject[] players;
    Camera cam;
    float currentSize;
    public float speed = 0.25f;

	// Use this for initialization
	void Start () {
        players= GameObject.FindGameObjectsWithTag("Player");
        cam = GetComponent<Camera>();
        currentSize = cam.orthographicSize;
    }
	
	// Update is called once per frame
	void Update () {

        if (_target == null || players.Length==0)
            return;

        float minX=0, minY=0, maxX=0, maxY=0;

        for (int i = 0; i < players.Length; i++)
        {
            if (i == 0)
            {
                minX = players[i].transform.position.x;

                minY = players[i].transform.position.y;

                maxX = players[i].transform.position.x;

                maxY = players[i].transform.position.y;
            }
            else
            {
                if (players[i].transform.position.x < minX)
                {
                    minX = players[i].transform.position.x;
                }
                if (players[i].transform.position.y < minY)
                {
                    minY = players[i].transform.position.y;
                }
                if (players[i].transform.position.x > maxX)
                {
                    maxX = players[i].transform.position.x;
                }
                if (players[i].transform.position.y > maxY)
                {
                    maxY = players[i].transform.position.y;
                }
            }
        }

        _target.position = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);
        this.transform.position = new Vector3(_target.position.x,_target.position.y,this.transform.position.z);

        currentSize =Mathf.Clamp(5 + Mathf.Abs(Mathf.Sqrt((maxX - minX) +(maxY - minY))),5,30);
        cam.orthographicSize = currentSize;
    }
}
