﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerID { P1,P2,P3,P4}

public class BearController : MonoBehaviour {
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    [HideInInspector] public bool backPulse = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck,topCheck,backCheck,frontCheck;


    private bool grounded = false;
    private bool onBack = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    public PlayerID playerID;
    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        //onBack= Physics2D.Linecast(transform.position, topCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        onBack = Physics2D.Linecast(transform.position, topCheck.position, 1 << LayerMask.NameToLayer("Ground")) ||
            Physics2D.Linecast(transform.position, backCheck.position, 1 << LayerMask.NameToLayer("Ground"))||
            Physics2D.Linecast(transform.position, frontCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        anim.SetBool("OnGround", grounded);
        if (Input.GetButtonDown(playerID+"Jump") )
        {
            if(grounded)
                jump = true;
            if(onBack && Mathf.Abs(transform.localEulerAngles.z)>45)            
                backPulse = true;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis(playerID + "Horizontal");
        
        if(onBack)
        {
            h = 0;
        }
        if(!grounded)
        {
            rb2d.AddTorque(-h * 45);
        }

        anim.SetBool("IsMoving", Mathf.Abs(h)>0.25f && grounded);

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        if (grounded)
        {
            if (h > 0 && !facingRight)
                Flip();
            else if (h < 0 && facingRight)
                Flip();
        }
        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }

        if (backPulse)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce*0.5f));
            rb2d.AddTorque(1080);
            backPulse = false;
        }
    }

    void BackSpin()
    {

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
